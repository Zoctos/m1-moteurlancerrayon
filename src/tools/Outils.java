package tools;

import java.util.ArrayList;
import java.util.Collections;

import fc.SimpleRaytracer.Geometry.Intersection;
import fc.SimpleRaytracer.Geometry.Triangle;
import fc.SimpleRaytracer.Math.Vector;

public class Outils {
	
	/*Permet de trier les triangles selon une coordonnee
	 * coord = 0 -> tri sur les x
	 * coord = 1 -> tri sur les y
	 * coord = 2 -> tri sur les z
	 */
	public static void triTriangles(ArrayList<Triangle> triangles, int coord){
		
		switch(coord){
		
			case 0: //x
				
				Collections.sort(triangles, (Triangle t1, Triangle t2) -> {
					if(t1.getMax().m_X > t2.getMax().m_X)
						return 1;
					else if(t1.getMax().m_X < t2.getMax().m_X)
						return -1;
					else
						return 0;
				});
				
				break;
			case 1 : //y
				
				Collections.sort(triangles, (Triangle t1, Triangle t2) -> {
					if(t1.getMax().m_Y > t2.getMax().m_Y)
						return 1;
					else if(t1.getMax().m_Y < t2.getMax().m_Y)
						return -1;
					else
						return 0;
				});
				
				break;
			case 2 : //z
				
				Collections.sort(triangles, (Triangle t1, Triangle t2) -> {
					if(t1.getMax().m_Z > t2.getMax().m_Z)
						return 1;
					else if(t1.getMax().m_Z < t2.getMax().m_Z)
						return -1;
					else
						return 0;
				});
				
				break;
		
		}
		
	}
	
	//Tri sur les intersections
	public static void triIntersection(ArrayList<Intersection> intersections){
		
		Collections.sort(intersections, (Intersection i1, Intersection i2) -> {
			if(i1.m_RayDistance > i2.m_RayDistance)
				return 1;
			else if(i1.m_RayDistance < i2.m_RayDistance)
				return -1;
			else
				return 0;
		});

		
	}
	
	//Retourne la valeur de la quantite lumineuse
	public static double quantiteLumineuse(Vector light, Vector normal){
		
		return Math.max(light.dot(normal), 0.0);
		
	}
	
	//Retourne un vecteur qui est la reflexion du vecteur v1 au vecteur v2
	public static Vector reflect(Vector v1, Vector v2){
		
		double angle = v1.dot(v2);
		
		Vector reflect = v2.sub(v1.mul(2*angle)); 
		
		return reflect;
		
	}
	
	//Retourne un vecteur refracte
	public static Vector refract(Vector incidence, Vector normal, double indiceEntree, double indiceSortie){
		
		double indiceDiv = indiceEntree/indiceSortie;
		double cos1 = incidence.dot(normal);
		
		double sin1 = Math.sqrt(1 - Math.pow(cos1, 2));
		double cos2 = Math.sqrt(1 - indiceDiv*Math.pow(sin1, 2)); 
		
		Vector refract = incidence.mul(indiceDiv).add(normal.mul(indiceDiv*cos1-cos2));
		
		return refract;
		
	}
	
	//Calcul de fresnel
	public static double fresnel(Vector incidence, Vector normal, Vector light, double indice){
		
		double fo = Math.pow((1-indice) / (1+indice), 2);
		
		Vector h = light.add(incidence).norm();
		
		double fresnel = fo + (1-fo)*Math.pow((1-light.dot(h)), 5);
				
		return fresnel;
		
	}

	//Calcul de schlick
	public static double schlick(Vector incidence, Vector normal, double indiceEntree, double indiceSortie){
		
		double rZero = (indiceEntree - indiceSortie) / (indiceEntree + indiceSortie);
		rZero = Math.pow(rZero, 2);
		
		double cos = Math.max(0, normal.dot(incidence));
		
		double schlick = rZero + ((1-rZero) * Math.pow((1 - cos), 5));
		
		return schlick;
	}
	
}
