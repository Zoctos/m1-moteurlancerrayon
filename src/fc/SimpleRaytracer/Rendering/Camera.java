package fc.SimpleRaytracer.Rendering;

import fc.SimpleRaytracer.Math.Vector;

public class Camera
{
	public Vector m_Position;
	public Vector m_ForwardDirection;
	public Vector m_RightDirection;
	public Vector m_UpDirection;
	
	public double m_FocalDistance = 0.8;

    public Camera(Vector pos, Vector lookAt)
    {
    	this.m_Position = pos;
    	
        Vector down = new Vector(0.0, -1.0, 0.0);
        this.m_ForwardDirection = lookAt.sub(pos).norm();
        this.m_RightDirection = down.cross(m_ForwardDirection).norm(); // .mul(1.5);
        this.m_UpDirection = m_RightDirection.cross(m_ForwardDirection).norm(); // .mul(1.5);
    }
}
