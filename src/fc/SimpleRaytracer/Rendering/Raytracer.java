package fc.SimpleRaytracer.Rendering;

import java.awt.image.BufferedImage;

import fc.SimpleRaytracer.Geometry.Intersection;
import fc.SimpleRaytracer.Geometry.Thing;
import fc.SimpleRaytracer.Material.Material;
import fc.SimpleRaytracer.Math.Ray;
import fc.SimpleRaytracer.Math.Vector;
import rendering.WindowRendering;

public class Raytracer
{
	private final int m_MaxDepth = 5;
	private double m_LastDist;
	public static int MAX_RAYONS_SECONDAIRE = 10;
	
	private Intersection intersect(Ray ray, Scene scene)
	{
		double closest = Double.POSITIVE_INFINITY;
		Intersection closestIntersection = null;
		
		for (Thing thing : scene.m_Things)
		{
			Intersection inter = thing.intersect(ray);
			if (inter != null && inter.m_RayDistance < closest)
			{
				closestIntersection = inter;
				closest = inter.m_RayDistance;
			}
		}
		
		return closestIntersection;
	}
	
	public Color traceRay(Ray ray, Scene scene, int depth, double a_RIndex)
	{
		Intersection isect = intersect(ray, scene);
		if (isect == null)
		{
			return Color.Background;
		}
		else
		{
			m_LastDist = isect.m_RayDistance;
			Color c = shade(scene.m_Camera, isect, scene, depth, a_RIndex);
			return c;
		}
	}
	
	private Color shade(Camera camera, Intersection isect, Scene scene, int depth, double a_RIndex)
	{
		Material material = isect.m_Thing.getMaterial();
		
		Color c = new Color(0,0,0);
        for (Light light : scene.m_Lights)
        {	
    		Color color = material.shade(camera, isect, light, this, a_RIndex, scene, depth);
    		c = c.add(color);
        }
        
        return c;
	}
	
	//Permet de lancer un rayon d'intersection pour une seule lumiere
	public Color traceRayOneLight(Ray ray, Scene scene, Light light, int depth, double a_RIndex){
		
		Intersection isect = intersect(ray, scene);
		if(isect == null){
			return Color.Background;
		}
		else{
			m_LastDist = isect.m_RayDistance;
			return shadeOneLight(scene.m_Camera, isect, scene, light, depth, a_RIndex);
		}
		
	}
	
	//Renvoie la couleur d'un point pour une seule lumiere
	private Color shadeOneLight(Camera camera, Intersection isect, Scene scene, Light light, int depth, double a_RIndex){
		
		Material material = isect.m_Thing.getMaterial();
		
		Color c = material.shade(camera, isect, light, this, a_RIndex, scene, depth);
		
		return c;
		
	}
	
	private Vector getPoint(int x, int y, Camera camera, int width, int height)
	{
		// Example x going from 0 to 640, excluded
		// Then dx is between -320.0 and 320.0 (excluded)
		double dx = (double)x - (width / 2.0);
		// Then dx is between -0.5 and 0.5 (excluded)
		dx /= (double)Math.max(width, height);
		double dy = (double)y - (height / 2.0);
		dy /= (double)Math.max(width, height);

        Vector result = camera.m_RightDirection.mul(dx).add(camera.m_UpDirection.mul(dy)).add(camera.m_ForwardDirection.mul(camera.m_FocalDistance)).norm();
        return result;
	}
	
	// Exercice A - expliquer brievement
	public void render(Scene scene, BufferedImage image)
	{
		int width = image.getWidth();
		int height = image.getHeight();
		
		for (int y=0; y < height; y++)
		{
			System.out.println("Raytracing scanline: " + y + "/" + height);
			for (int x=0; x < width; x++)
			{
				Vector dir = getPoint(x, y, scene.m_Camera, width, height);
				Color color = traceRay(new Ray(scene.m_Camera.m_Position, dir), scene, 0, /* a_RIndex initial */ 1.0);
				int rgb = color.toRGB();
				image.setRGB(x, height-y-1, rgb);
			}
			WindowRendering.setImage();
		}
		
		System.out.println("Complete.");
	}
}
