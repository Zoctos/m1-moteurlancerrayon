package fc.SimpleRaytracer.Rendering;

import fc.SimpleRaytracer.Geometry.Thing;

public class Scene
{
	public Thing[] m_Things;
	public Light[] m_Lights;
	public Camera m_Camera;
	
	public Scene(Thing[] things, Light[] lights, Camera camera)
	{
		this.m_Things = things;
		this.m_Lights = lights;
		this.m_Camera = camera;
	}
}
