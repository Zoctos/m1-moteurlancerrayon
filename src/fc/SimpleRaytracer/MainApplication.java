package fc.SimpleRaytracer;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Date;

import javax.imageio.ImageIO;

import com.owens.oobjloader.builder.Build;
import com.owens.oobjloader.builder.Face;
import com.owens.oobjloader.builder.FaceVertex;
import com.owens.oobjloader.builder.VertexNormal;
import com.owens.oobjloader.parser.Parse;

import fc.SimpleRaytracer.Geometry.CSGNode;
import fc.SimpleRaytracer.Geometry.Mesh;
import fc.SimpleRaytracer.Geometry.OctreeNode;
import fc.SimpleRaytracer.Geometry.Sphere;
import fc.SimpleRaytracer.Geometry.Thing;
import fc.SimpleRaytracer.Geometry.Triangle;
import fc.SimpleRaytracer.Material.Material;
import fc.SimpleRaytracer.Material.MaterialLaFortune;
import fc.SimpleRaytracer.Material.MaterialLambert;
import fc.SimpleRaytracer.Material.MaterialMiroir;
import fc.SimpleRaytracer.Material.MaterialPhong;
import fc.SimpleRaytracer.Material.MaterialVerre;
import fc.SimpleRaytracer.Material.MaterialColor;
import fc.SimpleRaytracer.Material.MaterialFactory;
import fc.SimpleRaytracer.Math.Vector;
import fc.SimpleRaytracer.Rendering.Camera;
import fc.SimpleRaytracer.Rendering.Color;
import fc.SimpleRaytracer.Rendering.Light;
import fc.SimpleRaytracer.Rendering.Raytracer;
import fc.SimpleRaytracer.Rendering.Scene;
import rendering.WindowRendering;

public class MainApplication
{
	
	public static int widthImage = 800;
	public static int heightImage = 800;
	public static BufferedImage bi;
	
	public static void main(String[] args)
	{
		
		bi = new BufferedImage(widthImage, heightImage, BufferedImage.TYPE_INT_RGB);
		
		Thread t = new Thread(){
			public void run(){
				WindowRendering wr = new WindowRendering();
				wr.startRendering(args, bi);
			}
		};
		t.start();
		
		
		OctreeNode link = parseObjFile("link.obj", new Vector(-2.0, 1.1, -8.0), MaterialFactory.materialPlastique(new Color(0.0, 0.2, 1.0)));
		OctreeNode link2 = parseObjFile("link.obj", new Vector(2.0, 1.1, -8.0), MaterialFactory.materialBronze());
		OctreeNode child = parseObjFile("childLink.obj", new Vector(-1.0, -0.3, -6.0), MaterialFactory.materialEmeraude());
		
		OctreeNode miroir = parseObjFile("plane.obj", new Vector(-2.0, -1.0, -10.0), MaterialFactory.materialMiroir());
		
		OctreeNode fond = parseObjFile("planeFond.obj", new Vector(0.0, 0.0, -10.0), MaterialFactory.materialBleu());
		OctreeNode arriere = parseObjFile("planeFond.obj", new Vector(0.0, 0.0, 2.0), MaterialFactory.materialCyan());
		OctreeNode gauche = parseObjFile("planeCote.obj", new Vector(-5.0, 0.0, -5.0), MaterialFactory.materialRouge());
		OctreeNode droite = parseObjFile("planeCote.obj", new Vector(5.0, 0.0, -5.0), MaterialFactory.materialVert());
		OctreeNode plafond = parseObjFile("plane.obj", new Vector(-2.0, 10.0, -5.0), MaterialFactory.materialJaune());
		
		OctreeNode cube = parseObjFile("cube.obj", new Vector(0.5, 1.0, -3.0), MaterialFactory.materialVerre());
		
		double k = 2.0;
		double y = 0.2;
		Thing[] things =
			{
				link,
				link2,
				child,
				fond,
				arriere,
				gauche,
				droite,
				plafond,
				miroir,
				cube,
			};
		
		Light[] lights =
			{
				//new Light(new Vector(0.0, 0.0, 0.0), new Color(1.0, 1.0, 1.0)),
				new Light(new Vector(10.0, 10.0, 0.0), new Color(1.0, 1.0, 1.0)),
				new Light(new Vector(-10.0, 10.0, 0.0), new Color(1.0, 1.0, 1.0)),
			};
		
		Camera camera = new Camera(new Vector(0.0, 1.0, 0.0), new Vector(0.0, 1.0, -10.0));
		
		Scene scene = new Scene(things, lights, camera);
		
		try
		{
			Raytracer raytracer = new Raytracer();
			raytracer.render(scene, bi);
		}
		catch (Exception e)
		{
			System.out.println(e.toString());
		}
	
		Date date = new Date();
		date.toInstant();
		
	    File outputfile = new File("result.png");
	    
	    try
	    {
	    	ImageIO.write(bi, "png", outputfile);
	    }
	    catch (IOException e)
	    {
	    	System.out.println("Error, IOException caught: " + e.toString());
	    }
	}
	
	public static OctreeNode parseObjFile(String filename, Vector pos, Material material)
	{
	    try
	    {
	        Build builder = new Build();
	        Parse obj = new Parse(builder, new File("obj/"+filename).toURI().toURL());
	        
	        ArrayList<Triangle> listTriangle = new ArrayList<Triangle>();
	        
	        // Enumeration des faces (souvent des triangles, mais peuvent comporter plus de sommets dans certains cas)
	        
	        for (int e = 0; e < builder.faces.size(); e++)
	        {
	        	Face face = builder.faces.get(e);
	        	
	        	// Parcours des triangles de cette face
	        	
	        	for (int i=1; i <= (face.vertices.size() - 2); i++)
	        	{
	        		int vertexIndex1 = face.vertices.get(0).index;
	        		int vertexIndex2 = face.vertices.get(i).index;
	        		int vertexIndex3 = face.vertices.get(i+1).index;
	        		
	        		FaceVertex vertex1 = builder.faceVerticeList.get(vertexIndex1);
	        		FaceVertex vertex2 = builder.faceVerticeList.get(vertexIndex2);
	        		FaceVertex vertex3 = builder.faceVerticeList.get(vertexIndex3);
	        		
	        		Vector s1 = new Vector(vertex1.v.x, vertex1.v.y, vertex1.v.z).add(pos);
	        		Vector s2 = new Vector(vertex2.v.x, vertex2.v.y, vertex2.v.z).add(pos);
	        		Vector s3 = new Vector(vertex3.v.x, vertex3.v.y, vertex3.v.z).add(pos);
	        		
	        		if(builder.verticesN.size() == 0){
	        			listTriangle.add(new Triangle(s1, s2, s3, material));
	        		}
	        		else{
		        		VertexNormal n1 = vertex1.n;
		        		VertexNormal n2 = vertex2.n;
		        		VertexNormal n3 = vertex3.n;
		        		
		        		listTriangle.add(new Triangle(s1, s2, s3, material, new Vector(n1.x, n1.y, n1.z), new Vector(n2.x, n2.y, n2.z), new Vector(n3.x, n3.y, n3.z)));
	        		}
	        		
	        	}
	        }
	        
	        Mesh m = new Mesh(listTriangle, material);
	        
	        return new OctreeNode(m.triangles, 0, null);
	        
	    }
	    catch (java.io.FileNotFoundException e)
	    {
	    	System.out.println("FileNotFoundException loading file "+filename+", e=" + e);
	        e.printStackTrace();
	    }
	    catch (java.io.IOException e)
	    {
	    	System.out.println("IOException loading file "+filename+", e=" + e);
	        e.printStackTrace();
	    }
	    return null;
	}
}
