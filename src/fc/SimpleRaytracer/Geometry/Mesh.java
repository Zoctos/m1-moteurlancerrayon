package fc.SimpleRaytracer.Geometry;

import java.util.ArrayList;

import fc.SimpleRaytracer.Material.Material;
import fc.SimpleRaytracer.Math.Ray;
import fc.SimpleRaytracer.Math.Vector;
import tools.Outils;

public class Mesh implements Thing {

	public ArrayList<Triangle> triangles;
	public Material material;
	
	public Vector center;

	public Mesh(ArrayList<Triangle> triangles, Material material){
		
		this.triangles = triangles;
		this.material = material;
		
	}
	
	@Override
	public Intersection intersect(Ray ray) {
		
		Intersection inter = null;
		
		for(Triangle t : triangles){
			
			Intersection tmp = t.intersect(ray);
			
			if(tmp != null && (inter == null || Math.abs(tmp.m_RayDistance) < Math.abs(inter.m_RayDistance))){
				inter = tmp;
			}
			
		}
		
		return inter;
	}

	@Override
	public Material getMaterial() {
		return material;
	}

	@Override
	public boolean isSolid() {
		return true;
	}
	
	public void setAtOrigin(){
		
		if(center == null){
			
			center = new Vector(0, 0, 0);
			
			for(Triangle t : triangles){
				
				center = center.add(t.p1).add(t.p2).add(t.p3);
				
			}
			
			center = center.div(triangles.size()*3);
			
		}
	
		translate(center);
		
	}
	
	public void translate(Vector translate){
		
		for(Triangle t : triangles){
			t.translate(translate);
		}
		
	}

	@Override
	public ArrayList<Intersection> allIntersect(Ray ray) {
		
		ArrayList<Intersection> inter = new ArrayList<Intersection>();
		
		for(Triangle t : triangles){
			
			Intersection tmp = t.intersect(ray);
			
			if(tmp != null){
				inter.add(tmp);
			}
			
		}
		Outils.triIntersection(inter);
		return inter;
		
	}
	
}
