package fc.SimpleRaytracer.Geometry;

import java.util.ArrayList;

import com.owens.oobjloader.builder.VertexNormal;

import fc.SimpleRaytracer.Material.Material;
import fc.SimpleRaytracer.Material.MaterialColor;
import fc.SimpleRaytracer.Math.Ray;
import fc.SimpleRaytracer.Math.Vector;
import fc.SimpleRaytracer.Rendering.Raytracer;
import tools.Outils;

public class Triangle implements Thing{

	public Vector p1, p2, p3;
	public Vector n1, n2, n3;
	public Material material;
	
	public Triangle(Vector p1, Vector p2, Vector p3, Material material){
		this.p1 = p1;
		this.p2 = p2;
		this.p3 = p3;
		this.material = material;
	}
	
	public Triangle(Vector p1, Vector p2, Vector p3, Material material, Vector n1, Vector n2, Vector n3){
		this.p1 = p1;
		this.p2 = p2;
		this.p3 = p3;
		this.material = material;
		this.n1 = n1;
		this.n2 = n2;
		this.n3 = n3;
	}
	
	public Vector getNormalFace(){
		
		Vector v1v2 = p2.sub(p1);
		Vector v1v3 = p3.sub(p1);
				
		return v1v2.cross(v1v3);
		
	}
	
	@Override
	public Intersection intersect(Ray ray) {
		
		Vector normalFace = getNormalFace();
		
		double facedotRayDirection = normalFace.dot(ray.m_Direction);
		
		Vector P = ray.m_Origin.sub(p1);
		
		double invFaceDotP = -normalFace.dot(P);
		
		//Si l'objet est derri�re
		double t = invFaceDotP / facedotRayDirection;
		if(t < 0)
			return null;
		
		Vector pInter = ray.m_Origin.add(ray.m_Direction.mul(t));
		
		//Contient le segment entre le point d'intersection et un sommet du triangle
		Vector segPSom;
		
		//On recupere le premier segment du triangle et on calcul l'angle entre le point d'intersection et le segment
		Vector cote1 = p2.sub(p1);
		Vector vp1 = pInter.sub(p1);
		segPSom = cote1.cross(vp1);
		
		double aire = normalFace.dot(normalFace);
		double u = normalFace.dot(segPSom);
		
		//Si l'angle est inf�rieur � 0, alors le point est en dehors du triangle
		if(normalFace.dot(segPSom) < 0)
			return null;
		
		//On fait la m�me chose avec les autres segments
		Vector cote2 = p3.sub(p2);
		Vector vp2 = pInter.sub(p2);
		segPSom = cote2.cross(vp2);
		
		double v = normalFace.dot(segPSom);
		
		if(normalFace.dot(segPSom) < 0)
			return null;
		
		Vector cote3 = p1.sub(p3);
		Vector vp3 = pInter.sub(p3);
		segPSom = cote3.cross(vp3);
		
		double w = normalFace.dot(segPSom);
		
		if(normalFace.dot(segPSom) < 0)
			return null;
		
		Vector N;
		
		if(n1 != null)
			N = n3.mul(u/aire).add(n1.mul(v/aire)).add(n2.mul(w/aire));
		else
			N = normalFace;
		
		double dist = Math.abs(pInter.sub(ray.m_Origin).length());
		
		//On renvoie l'intersection
		return new Intersection(this, ray, dist, N);
	}

	@Override
	public Material getMaterial() {
		return this.material;
	}

	@Override
	public boolean isSolid() {
		return true;
	}
	
	public Vector getMin(){
		
		Vector min = new Vector(p1.m_X, p1.m_Y, p1.m_Z);
		
		if(min.m_X > p2.m_X)
			min.m_X = p2.m_X;
		if(min.m_X > p3.m_X)
			min.m_X = p3.m_X;
		
		if(min.m_Y > p2.m_Y)
			min.m_Y = p2.m_Y;
		if(min.m_Y > p3.m_Y)
			min.m_Y = p3.m_Y;
		
		if(min.m_Z > p2.m_Z)
			min.m_Z = p2.m_Z;
		if(min.m_Z > p3.m_Z)
			min.m_Z = p3.m_Z;
		
		return min;
		
	}
	
	public Vector getMax(){
		
		Vector max = new Vector(p1.m_X, p1.m_Y, p1.m_Z);
		
		if(max.m_X < p2.m_X)
			max.m_X = p2.m_X;
		if(max.m_X < p3.m_X)
			max.m_X = p3.m_X;
		
		if(max.m_Y < p2.m_Y)
			max.m_Y = p2.m_Y;
		if(max.m_Y < p3.m_Y)
			max.m_Y = p3.m_Y;
		
		if(max.m_Z < p2.m_Z)
			max.m_Z = p2.m_Z;
		if(max.m_Z < p3.m_Z)
			max.m_Z = p3.m_Z;
		
		return max;
		
	}

	public void translate(Vector translate){
		
		p1 = p1.sub(translate);
		p2 = p2.sub(translate);
		p3 = p3.sub(translate);
	}

	@Override
	public ArrayList<Intersection> allIntersect(Ray ray) {
		
		ArrayList<Intersection> inter = new ArrayList<Intersection>();
		
		Intersection tmp = intersect(ray);
		
		if(tmp != null)
			inter.add(tmp);
		Outils.triIntersection(inter);
		return inter;
	}
	
}
