package fc.SimpleRaytracer.Geometry;

import fc.SimpleRaytracer.Math.Ray;
import fc.SimpleRaytracer.Math.Vector;

public class AABB
{
	private Vector m_Min;
	private Vector m_Max;
	
	public AABB()
	{
		m_Min = new Vector(Float.POSITIVE_INFINITY, Float.POSITIVE_INFINITY, Float.POSITIVE_INFINITY);
		m_Max = new Vector(Float.NEGATIVE_INFINITY, Float.NEGATIVE_INFINITY, Float.NEGATIVE_INFINITY);
	}
	
	public AABB(AABB rhs)
	{
		m_Min = new Vector(rhs.getMin());
		m_Max = new Vector(rhs.getMax());
	}
	
	public AABB(Vector min, Vector max)
	{
		m_Min = min;
		m_Max = max;
	}
	
	public Vector getMin()
	{
		return m_Min;
	}
	
	public Vector getMax()
	{
		return m_Max;
	}
	
	public void enlarge(Vector v)
	{
		m_Min.m_X = Math.min(m_Min.m_X, v.m_X);
		m_Min.m_Y = Math.min(m_Min.m_Y, v.m_Y);
		m_Min.m_Z = Math.min(m_Min.m_Z, v.m_Z);
		
		m_Max.m_X = Math.max(m_Max.m_X, v.m_X);
		m_Max.m_Y = Math.max(m_Max.m_Y, v.m_Y);
		m_Max.m_Z = Math.max(m_Max.m_Z, v.m_Z);
	}
	
	public void enlarge(AABB rhs)
	{
		enlarge(rhs.getMin());
		enlarge(rhs.getMax());
	}
	
	public void addMargin(Vector margin)
	{
		m_Min = m_Min.sub(margin);
		m_Max = m_Max.add(margin);
	}
	
	public AABB grow(AABB rhs)
	{
		//AABB box = new AABB(new Vector(getMin()), new Vector(getMax()));
		
		this.enlarge(rhs.getMin());
		this.enlarge(rhs.getMax());
		
		return this; //box;
	}
	
	public Vector getCenter()
	{
		return m_Max.add(m_Min).div(2.0f);
	}
	
	public Vector getBoxHalfSize()
	{
		return m_Max.sub(getCenter());
	}
	
	public double getSurfaceArea()
	{
		Vector s = getMax().sub(getMin());
		return (s.m_X*s.m_Y + s.m_X*s.m_Z + s.m_Y*s.m_Z) * 2.0;
	}
	
	/*======================== X-tests ========================*/

	private boolean AXISTEST_X01(Vector v0, Vector v1, Vector v2, double a, double b, double fa, double fb)
	{
		double p0, p2, min, max, rad;
		
		p0 = a*v0.m_Y - b*v0.m_Z;
		p2 = a*v2.m_Y - b*v2.m_Z;
		if (p0<p2) {min=p0; max=p2;} else {min=p2; max=p0;}
		rad = fa * getBoxHalfSize().m_Y + fb * getBoxHalfSize().m_Z;
		if (min>rad || max<-rad)
			return false;
		return true;
	}

	private boolean AXISTEST_X2(Vector v0, Vector v1, Vector v2, double a, double b, double fa, double fb)
	{
		double p0, p1, min, max, rad;
		
		p0 = a*v0.m_Y - b*v0.m_Z;
		p1 = a*v1.m_Y - b*v1.m_Z;

        if(p0<p1) {min=p0; max=p1;} else {min=p1; max=p0;}
		rad = fa * getBoxHalfSize().m_Y + fb * getBoxHalfSize().m_Z;
		if(min>rad || max<-rad)
			return false;
		return true;
	}

	/*======================== Y-tests ========================*/

	private boolean AXISTEST_Y02(Vector v0, Vector v1, Vector v2, double a, double b, double fa, double fb)
	{
		double p0, p2, min, max, rad;
		
		p0 = -a*v0.m_X + b*v0.m_Z;
		p2 = -a*v2.m_X + b*v2.m_Z;

        if(p0<p2) {min=p0; max=p2;} else {min=p2; max=p0;}
		rad = fa * getBoxHalfSize().m_X + fb * getBoxHalfSize().m_Z;
		if(min>rad || max<-rad)
			return false;
		return true;
	}

	private boolean AXISTEST_Y1(Vector v0, Vector v1, Vector v2, double a, double b, double fa, double fb)
	{
		double p0, p1, min, max, rad;
		
		p0 = -a*v0.m_X + b*v0.m_Z;
		p1 = -a*v1.m_X + b*v1.m_Z;
		
        if(p0<p1) {min=p0; max=p1;} else {min=p1; max=p0;}
		rad = fa * getBoxHalfSize().m_X + fb * getBoxHalfSize().m_Z;
		if(min>rad || max<-rad)
			return false;
		return true;
	}

	/*======================== Z-tests ========================*/

	private boolean AXISTEST_Z12(Vector v0, Vector v1, Vector v2, double a, double b, double fa, double fb)
	{
		double p2, p1, min, max, rad;
		
		p1 = a*v1.m_X - b*v1.m_Y;
		p2 = a*v2.m_X - b*v2.m_Y;

		if(p2<p1) {min=p2; max=p1;} else {min=p1; max=p2;}
		rad = fa * getBoxHalfSize().m_X + fb * getBoxHalfSize().m_Y;
		if(min>rad || max<-rad)
			return false;
		return true;
	}

	private boolean AXISTEST_Z0(Vector v0, Vector v1, Vector v2, double a, double b, double fa, double fb)
	{
		double p0, p1, min, max, rad;
		
		p0 = a*v0.m_X - b*v0.m_Y;
		p1 = a*v1.m_X - b*v1.m_Y;

		if(p0<p1) {min=p0; max=p1;} else {min=p1; max=p0;}
		rad = fa * getBoxHalfSize().m_X + fb * getBoxHalfSize().m_Y;
		if(min>rad || max<-rad)
			return false;
		return true;
	}
	
	private void FINDMINMAX(double x0, double x1, double x2, double[] minmax)
	{
		minmax[0] = minmax[1] = x0;
		if (x1<minmax[0]) minmax[0]=x1;
		if (x1>minmax[1]) minmax[1]=x1;
		if (x2<minmax[0]) minmax[0]=x2;
		if (x2>minmax[1]) minmax[1]=x2;
	}
	  
	//
	// Original code:
	// http://fileadmin.cs.lth.se/cs/Personal/Tomas_Akenine-Moller/code/tribox3.txt
	//
	public boolean intersectTriangle(Triangle t)
	{
		Vector boxcenter = getCenter();
		Vector boxhalfsize = getMax().sub(boxcenter);
		Vector[] triverts = { t.p1, t.p2, t.p3 };
		
		//int triBoxOverlap(float boxcenter[3],float boxhalfsize[3],float triverts[3][3])

		/*    use separating axis theorem to test overlap between triangle and box */
		/*    need to test for overlap in these directions: */
		/*    1) the {x,y,z}-directions (actually, since we use the AABB of the triangle */
		/*       we do not even need to test these) */
		/*    2) normal of the triangle */
		/*    3) crossproduct(edge from tri, {x,y,z}-directin) */
		/*       this gives 3x3=9 more tests */
		Vector v0, v1, v2;

		//   float axis[3];

		double[] minmax = new double[2];
		double p0,p1,p2,rad,fex,fey,fez;		// -NJMP- "d" local variable removed
		Vector normal,e0,e1,e2;
		
		/* This is the fastest branch on Sun */
		/* move everything so that the boxcenter is in (0,0,0) */
		
		v0 = triverts[0].sub(boxcenter);
		v1 = triverts[1].sub(boxcenter);
		v2 = triverts[2].sub(boxcenter);
		
		/* compute triangle edges */
		
		e0 = v1.sub(v0);      /* tri edge 0 */
		e1 = v2.sub(v1);      /* tri edge 1 */
		e2 = v0.sub(v2);      /* tri edge 2 */
		
		/* Bullet 3:  */
		
		/*  test the 9 tests first (this was faster) */
		
		fex = Math.abs(e0.m_X);
		fey = Math.abs(e0.m_Y);
		fez = Math.abs(e0.m_Z);
		
		if (!AXISTEST_X01(v0, v1, v2, e0.m_Z, e0.m_Y, fez, fey)) return false;
		if (!AXISTEST_Y02(v0, v1, v2, e0.m_Z, e0.m_X, fez, fex)) return false;
		if (!AXISTEST_Z12(v0, v1, v2, e0.m_Y, e0.m_X, fey, fex)) return false;
		
		fex = Math.abs(e1.m_X);
		fey = Math.abs(e1.m_Y);
		fez = Math.abs(e1.m_Z);
		
		if (!AXISTEST_X01(v0, v1, v2, e1.m_Z, e1.m_Y, fez, fey)) return false;
		if (!AXISTEST_Y02(v0, v1, v2, e1.m_Z, e1.m_X, fez, fex)) return false;
		if (!AXISTEST_Z0(v0, v1, v2, e1.m_Y, e1.m_X, fey, fex)) return false;
		
		fex = Math.abs(e2.m_X);
		fey = Math.abs(e2.m_Y);
		fez = Math.abs(e2.m_Z);
		
		if (!AXISTEST_X2(v0, v1, v2, e2.m_Z, e2.m_Y, fez, fey)) return false;
		if (!AXISTEST_Y1(v0, v1, v2, e2.m_Z, e2.m_X, fez, fex)) return false;
		if (!AXISTEST_Z12(v0, v1, v2, e2.m_Y, e2.m_X, fey, fex)) return false;
		
		/* Bullet 1: */
		/*  first test overlap in the {x,y,z}-directions */
		/*  find min, max of the triangle each direction, and test for overlap in */
		/*  that direction -- this is equivalent to testing a minimal AABB around */
		/*  the triangle against the AABB */
		
		/* test in X-direction */
		FINDMINMAX(v0.m_X,v1.m_X,v2.m_X, minmax);
		if(minmax[0]>boxhalfsize.m_X || minmax[1]<-boxhalfsize.m_X) return false;
		
		/* test in Y-direction */
		FINDMINMAX(v0.m_Y,v1.m_Y,v2.m_Y, minmax);
		if(minmax[0]>boxhalfsize.m_Y || minmax[1]<-boxhalfsize.m_Y) return false;
		
		/* test in Z-direction */
		FINDMINMAX(v0.m_Z,v1.m_Z,v2.m_Z, minmax);
		if(minmax[0]>boxhalfsize.m_Z || minmax[1]<-boxhalfsize.m_Z) return false;
		
		/* Bullet 2: */
		/*  test if the box intersects the plane of the triangle */
		/*  compute plane equation of triangle: normal*x+d=0 */
		
		normal = e0.cross(e1);
		
		// -NJMP- (line removed here)
		if (!intersectPlane(normal,v0,boxhalfsize))
			return false;	// -NJMP-

		return true;   /* box and triangle overlaps */
	}
	
	public boolean intersectPlane(Vector normal, Vector vert, Vector maxbox)	// -NJMP-
	{
		int q;
		
		Vector vmin = new Vector(0,0,0);
		Vector vmax = new Vector(0,0,0);
		double v;
		
		for(q=0;q<=2;q++)
		{
			v = vert.getCoord(q);					// -NJMP-
			if(normal.getCoord(q)>0.0f)
			{
				vmin.setCoord(q, -maxbox.getCoord(q) - v);	// -NJMP-
				vmax.setCoord(q,  maxbox.getCoord(q) - v);	// -NJMP-
			}
			else
			{
				vmin.setCoord(q,  maxbox.getCoord(q) - v);	// -NJMP-
				vmax.setCoord(q, -maxbox.getCoord(q) - v);	// -NJMP-
			}
		}
		
		if (normal.dot(vmin)>0.0f) return false;	// -NJMP-
		if (normal.dot(vmax)>=0.0f) return true;	// -NJMP-
		
		return false;
	}
	
	//
	// See:
	// http://www.scratchapixel.com/lessons/3d-basic-rendering/minimal-ray-tracer-rendering-simple-shapes/ray-box-intersection
	//
	public boolean intersectRay(Ray r, double[] tminmax) // t : array of 2 floats containing min and max of Ray parameter value if intersection is found. Can be null.
	{
		double tmin = (m_Min.m_X - r.m_Origin.m_X) / r.m_Direction.m_X; 
		double tmax = (m_Max.m_X - r.m_Origin.m_X) / r.m_Direction.m_X;
		
		double temp;
		 
		if (tmin > tmax)
		{
			// Swap tmin and tmax
			temp = tmin;
			tmin = tmax;
			tmax = temp;
		}
		 
		double tymin = (m_Min.m_Y - r.m_Origin.m_Y) / r.m_Direction.m_Y; 
		double tymax = (m_Max.m_Y - r.m_Origin.m_Y) / r.m_Direction.m_Y; 
		 
		if (tymin > tymax)
		{
			// Swap tymin and tymax
			temp = tymin;
			tymin = tymax;
			tymax = temp;
		}
		 
		if ((tmin > tymax) || (tymin > tmax)) 
			return false; 
		 
		if (tymin > tmin) 
			tmin = tymin; 
		 
		if (tymax < tmax) 
			tmax = tymax; 
		 
		double tzmin = (m_Min.m_Z - r.m_Origin.m_Z) / r.m_Direction.m_Z; 
		double tzmax = (m_Max.m_Z - r.m_Origin.m_Z) / r.m_Direction.m_Z; 
		 
		if (tzmin > tzmax)
		{
			// Swap tzmin and tzmax
			temp = tzmin;
			tzmin = tzmax;
			tzmax = temp;
		}
		 
		if ((tmin > tzmax) || (tzmin > tmax)) 
			return false; 
		 
		if (tzmin > tmin) 
			tmin = tzmin; 
		 
		if (tzmax < tmax) 
			tmax = tzmax;
		
		if (tminmax != null)
		{
			tminmax[0] = tmin;
			tminmax[1] = tmax;
		}
		 
		return true; 
	}
	
	public boolean intersectRay_(Ray ray, double[] tminmax) //, float minDist, float maxDist)
	{
		Vector min = m_Min;
		Vector max = m_Max;
        Vector invDir = new Vector(1f / ray.m_Direction.m_X, 1f / ray.m_Direction.m_Y, 1f / ray.m_Direction.m_Z);

        boolean signDirX = invDir.m_X < 0;
        boolean signDirY = invDir.m_Y < 0;
        boolean signDirZ = invDir.m_Z < 0;

        Vector bbox = signDirX ? max : min;
        double tmin = (bbox.m_X - ray.m_Origin.m_X) * invDir.m_X;
        bbox = signDirX ? min : max;
        double tmax = (bbox.m_X - ray.m_Origin.m_X) * invDir.m_X;
        bbox = signDirY ? max : min;
        double tymin = (bbox.m_Y - ray.m_Origin.m_Y) * invDir.m_Y;
        bbox = signDirY ? min : max;
        double tymax = (bbox.m_Y - ray.m_Origin.m_Y) * invDir.m_Y;

        if ((tmin > tymax) || (tymin > tmax)) {
            return false;
        }
        if (tymin > tmin) {
            tmin = tymin;
        }
        if (tymax < tmax) {
            tmax = tymax;
        }

        bbox = signDirZ ? max : min;
        double tzmin = (bbox.m_Z - ray.m_Origin.m_Z) * invDir.m_Z;
        bbox = signDirZ ? min : max;
        double tzmax = (bbox.m_Z - ray.m_Origin.m_Z) * invDir.m_Z;

        if ((tmin > tzmax) || (tzmin > tmax)) {
            return false;
        }
        if (tzmin > tmin) {
            tmin = tzmin;
        }
        if (tzmax < tmax) {
            tmax = tzmax;
        }
        //if ((tmin < maxDist) && (tmax > minDist)) {
            return true; //ray.start.add(ray.dir.mul((float)tmin)); //.getPointAtDistance(tmin);
        //}
        //return null;
    }
	
	@Override
	public int hashCode()
	{
		return (int)m_Min.m_X;
	}
	
	@Override
	public boolean equals(Object o)
	{
		if (!(o instanceof AABB))
			return false;
		AABB rhs = (AABB)o;
		return rhs.getMin().equals(m_Min) && rhs.getMax().equals(m_Max);
	}
}