package fc.SimpleRaytracer.Geometry;

import java.util.ArrayList;

import fc.SimpleRaytracer.Material.Material;
import fc.SimpleRaytracer.Math.Ray;
import tools.Outils;

public class CSGNode implements Thing{

	public static final int TYPE_UNION = 0;
	public static final int TYPE_INTER = 1;
	public static final int TYPE_DIFF = 2;
	Thing obj1, obj2;
	int type;
	
	//Constructeur
	public CSGNode(Thing obj1, Thing obj2, int type){
		
		this.obj1 = obj1;
		this.obj2 = obj2;
		
		this.type = type;
		
	}
	
	@Override
	public Intersection intersect(Ray ray) {
		
		Intersection inter = null;
		
		Intersection inter1 = obj1.intersect(ray);
		Intersection inter2 = obj2.intersect(ray);
		
		switch(type){
		
			case TYPE_UNION :
				
				if(inter1 != null && inter2 != null){
					
					if(inter1.m_RayDistance < inter2.m_RayDistance)
						inter = inter1;
					else
						inter = inter2;
					
				}
				else if(inter1 != null)
					inter = inter1;
				else if(inter2 != null)
					inter = inter2;
			
				break;
		
			case TYPE_INTER :
				
				//Pour inter et diff on a besoin de conna�tre les intersections avec les diff�rents triangles de l'objet pour le rayon afin de d�terminer
				//si on est dans l'objet ou si on est � l'ext�rieur de l'objet
				
				ArrayList<Intersection> listInter1 = obj1.allIntersect(ray);
				ArrayList<Intersection> listInter2 = obj2.allIntersect(ray);
				
				for(Intersection int1 : listInter1){
					
					boolean isInside = false;
					
					for(int i = 0; i < listInter2.size()-1; i++){
						
						Intersection i21 = listInter2.get(i);
						Intersection i22 = listInter2.get(i+1);
						
						if(i21.m_RayDistance <= int1.m_RayDistance && i22.m_RayDistance >= int1.m_RayDistance)
							isInside = true;
						
					}
					
					if(isInside){
						if(inter == null)
							inter = int1;
						else if(inter.m_RayDistance > int1.m_RayDistance)
							inter = int1;
							
					}
					
				}
				
				if(inter == null){
					
					for(Intersection int2 : listInter2){
						
						boolean isInside = false;
						
						for(int i = 0; i < listInter1.size()-1; i++){
							
							Intersection i11 = listInter1.get(i);
							Intersection i12 = listInter1.get(i+1);
							
							if(i11.m_RayDistance <= int2.m_RayDistance && i12.m_RayDistance >= int2.m_RayDistance)
								isInside = true;
							
						}
						
						if(isInside){
							if(inter == null)
								inter = int2;
							else if(inter.m_RayDistance > int2.m_RayDistance)
								inter = int2;
								
						}
						
					}
					
				}
				
				break;
		
			case TYPE_DIFF :
				
				ArrayList<Intersection> listDiff1 = obj1.allIntersect(ray);
				ArrayList<Intersection> listDiff2 = obj2.allIntersect(ray);
				
				for(Intersection int1 : listDiff1){
					
					boolean isInside = false;
					
					for(int i = 0; i < listDiff2.size()-1; i++){
						
						Intersection i21 = listDiff2.get(i);
						Intersection i22 = listDiff2.get(i+1);
						
						if(i21.m_RayDistance <= int1.m_RayDistance && i22.m_RayDistance >= int1.m_RayDistance)
							isInside = true;
						
					}
					
					if(!isInside){
						if(inter == null)
							inter = int1;
						else if(inter.m_RayDistance > int1.m_RayDistance)
							inter = int1;
							
					}
					
				}
	
				break;
				
		}
		
		return inter;
	}

	@Override
	public Material getMaterial() {
		return null;
	}

	@Override
	public boolean isSolid() {
		return false;
	}


	@Override
	public ArrayList<Intersection> allIntersect(Ray ray) {
			
		ArrayList<Intersection> inter = new ArrayList<Intersection>();
		
		ArrayList<Intersection> listInter1 = obj1.allIntersect(ray);
		ArrayList<Intersection> listInter2 = obj2.allIntersect(ray);
		
		switch(type){
		
			case TYPE_UNION :
				
				inter.addAll(listInter1);
				inter.addAll(listInter2);
			
				break;
		
			case TYPE_INTER :
				
				//Pour inter et diff on a besoin de conna�tre les intersections avec les diff�rents triangles de l'objet pour le rayon afin de d�terminer
				//si on est dans l'objet ou si on est � l'ext�rieur de l'objet
				
				for(Intersection int1 : listInter1){
					
					boolean isInside = false;
					
					for(int i = 0; i < listInter2.size()-1; i++){
						
						Intersection i21 = listInter2.get(i);
						Intersection i22 = listInter2.get(i+1);
						
						if(i21.m_RayDistance <= int1.m_RayDistance && i22.m_RayDistance >= int1.m_RayDistance)
							isInside = true;
						
					}
					
					if(isInside){
						inter.add(int1);
					}
					
				}
				
				if(inter.size() == 0){
					
					for(Intersection int2 : listInter2){
						
						boolean isInside = false;
						
						for(int i = 0; i < listInter1.size()-1; i++){
							
							Intersection i11 = listInter1.get(i);
							Intersection i12 = listInter1.get(i+1);
							
							if(i11.m_RayDistance <= int2.m_RayDistance && i12.m_RayDistance >= int2.m_RayDistance)
								isInside = true;
							
						}
						
						if(isInside){
							inter.add(int2);	
						}
						
					}
					
				}
				
				break;
		
			case TYPE_DIFF :
				
				for(Intersection int1 : listInter1){
					
					boolean isInside = false;
					
					for(int i = 0; i < listInter2.size()-1; i++){
						
						Intersection i21 = listInter2.get(i);
						Intersection i22 = listInter2.get(i+1);
						
						if(i21.m_RayDistance <= int1.m_RayDistance && i22.m_RayDistance >= int1.m_RayDistance)
							isInside = true;
						
					}
					
					if(!isInside){
						inter.add(int1);	
					}
					
				}
	
				break;
				
		}
		
		Outils.triIntersection(inter);
		
		return inter;
	}
		
}
