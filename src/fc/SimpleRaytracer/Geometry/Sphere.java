package fc.SimpleRaytracer.Geometry;

import java.util.ArrayList;

import fc.SimpleRaytracer.Material.Material;
import fc.SimpleRaytracer.Math.Ray;
import fc.SimpleRaytracer.Math.Vector;
import tools.Outils;

public class Sphere implements Thing{

	public Vector origine;
	public double rayon;
	public Material material;
	
	public Sphere(Vector origine, double rayon, Material material){
		this.origine = origine;
		this.rayon = rayon;
		this.material = material;
	}
	
	public Vector getNormal(Vector point){
		
		Vector normal = point.sub(origine);
		
		return normal;
	}
	
	@Override
	public Intersection intersect(Ray ray) {
		
		double dx = ray.m_Direction.m_X;
		double dy = ray.m_Direction.m_Y;
		double dz = ray.m_Direction.m_Z;
		
		double ox = ray.m_Origin.m_X;
		double oy = ray.m_Origin.m_Y;
		double oz = ray.m_Origin.m_Z;
		
		double a = Math.pow(dx, 2.0) + Math.pow(dy, 2.0) + Math.pow(dz, 2.0);
		double b = 2.0* (dx * (ox - origine.m_X) + dy * (oy - origine.m_Y) + dz * (oz - origine.m_Z));
		double c = Math.pow(ox-origine.m_X, 2.0) + Math.pow(oy-origine.m_Y, 2.0) + Math.pow(oz - origine.m_Z, 2.0) - Math.pow(rayon, 2.0);
		
		double delta = Math.pow(b, 2.0) - 4.0 * a * c;
		
		//Si delta == 0 on a une seule intersection
		if(delta == 0){
			
			double t = -b/(2.0*a);
			
			Vector point = ray.m_Origin.add(ray.m_Direction.mul(t));
			
			return new Intersection(this, ray, Math.abs(t), getNormal(point));
			
		}
		if(delta > 0){ //Sinon si delta > 0 on a deux intersections
			
			double t1 = (-b + Math.sqrt(delta))/(2.0*a);
			double t2 = (-b - Math.sqrt(delta))/(2.0*a);
			
			double t = t1;
			if(t1 > t2)
				t = t2;
			
			Vector point = ray.m_Origin.add(ray.m_Direction.mul(t));
			
			Intersection p = new Intersection(this, ray, Math.abs(t), getNormal(point));

			return p;
			
		}
		else
			return null;
	}

	@Override
	public Material getMaterial() {
		return material;
	}

	@Override
	public boolean isSolid() {
		return false;
	}

	@Override
	public ArrayList<Intersection> allIntersect(Ray ray) {

		double dx = ray.m_Direction.m_X;
		double dy = ray.m_Direction.m_Y;
		double dz = ray.m_Direction.m_Z;
		
		double ox = ray.m_Origin.m_X;
		double oy = ray.m_Origin.m_Y;
		double oz = ray.m_Origin.m_Z;
		
		double a = Math.pow(dx, 2) + Math.pow(dy, 2) + Math.pow(dz, 2);
		double b = 2* (dx * (ox - origine.m_X) + dy * (oy - origine.m_Y) + dz * (oz - origine.m_Z));
		double c = Math.pow(ox-origine.m_X, 2) + Math.pow(oy-origine.m_Y, 2) + Math.pow(oz - origine.m_Z, 2) - Math.pow(rayon, 2);
		
		double delta = Math.pow(b, 2) - 4 * a * c;
		
		ArrayList<Intersection> inter = new ArrayList<Intersection>();
		
		if(delta == 0){
			
			double t = -b/(2*a);
			
			Vector point = ray.m_Origin.add(ray.m_Direction.mul(t));
			
			inter.add(new Intersection(this, ray, Math.abs(t), getNormal(point)));
			
		}
		if(delta > 0){
			
			double t1 = (-b + Math.sqrt(delta))/(2*a);
			double t2 = (-b - Math.sqrt(delta))/(2*a);
			
			double t = t1;
			if(t1 > t2){
				t = t2;
				t2 = t1;
				t1 = t;
			}
			
			Vector point1 = ray.m_Origin.add(ray.m_Direction.mul(t1));
			Vector point2 = ray.m_Origin.add(ray.m_Direction.mul(t2));
			
			Intersection p1 = new Intersection(this, ray, Math.abs(t1), getNormal(point1));
			Intersection p2 = new Intersection(this, ray, Math.abs(t2), getNormal(point2));
			
			inter.add(p1);
			inter.add(p2);
			
		}
		
		return inter;
		
	}

}
