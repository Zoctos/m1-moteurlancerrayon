package fc.SimpleRaytracer.Geometry;

import java.util.ArrayList;

import fc.SimpleRaytracer.Material.Material;
import fc.SimpleRaytracer.Math.Ray;
import fc.SimpleRaytracer.Math.Vector;

public interface Thing
{
	public Intersection intersect(Ray ray);
	public ArrayList<Intersection> allIntersect(Ray ray);
	public Material getMaterial();
	public boolean isSolid();
}
