package fc.SimpleRaytracer.Geometry;

import java.util.ArrayList;

import fc.SimpleRaytracer.Material.Material;
import fc.SimpleRaytracer.Material.MaterialPhong;
import fc.SimpleRaytracer.Material.MaterialColor;
import fc.SimpleRaytracer.Math.Ray;
import fc.SimpleRaytracer.Math.Vector;
import tools.Outils;

public class OctreeNode implements Thing{
	
	public final int OCTREE_MIN_TRIANGLES = 5;
	public final int OCTREE_MAX_PROFONDEUR = 10;
	
	public ArrayList<Triangle> triangles;
	public OctreeNode parent;
	public int positionId;
	public AABB boite;
	ArrayList<OctreeNode> enfants;
	
	//Constructeur
	public OctreeNode(ArrayList<Triangle> triangles, int positionId, OctreeNode parent){
		
		if(parent == null)
			System.out.println("Construction OctreeNode");
		
		this.positionId = positionId;
		this.parent = parent;
		
		if(positionId == 0){
			
			boite = new AABB(getMin(triangles), getMax(triangles));
			
		}
		
		ArrayList<Triangle> trianglesIntersectes = getIntersectedTriangle(triangles);
		
		if(getProfondeur() >= OCTREE_MAX_PROFONDEUR || trianglesIntersectes.size() <= OCTREE_MIN_TRIANGLES){
			this.triangles = trianglesIntersectes;
		}
		else{
			
			enfants = new ArrayList<OctreeNode>();
			
			for(int i = 1; i < 9; i++){
				enfants.add(new OctreeNode(trianglesIntersectes, i, this));
			}
			
		}
		
	}
	
	//Renvoie la profondeur du noeud actuel
	public int getProfondeur(){
		
		if(parent == null)
			return 1;
		else
			return 1+parent.getProfondeur();
		
	}
	
	//Renvoie le point minimum de la boite representant le noeud
	public Vector getMin(ArrayList<Triangle> listTriangles){
		
		Vector minPoint = new Vector(listTriangles.get(0).p1.getCoord(0), listTriangles.get(0).p1.getCoord(1), listTriangles.get(0).p1.getCoord(2));
		
		for(Triangle t : listTriangles){
			
			Vector p = t.getMin();
			
			if(minPoint.m_X > p.m_X){
				minPoint.m_X = p.getCoord(0);
			}
			
			if(minPoint.m_Y > p.m_Y){
				minPoint.m_Y = p.getCoord(1);
			}
			
			if(minPoint.m_Z > p.m_Z){
				minPoint.m_Z = p.getCoord(2);
			}
		}
		
		return minPoint;
		
	}
	
	//Renvoie le point maximum de la boite representant le noeud
	public Vector getMax(ArrayList<Triangle> listTriangles){
		
		Vector maxPoint = new Vector(listTriangles.get(0).p1.getCoord(0), listTriangles.get(0).p1.getCoord(1), listTriangles.get(0).p1.getCoord(2));
		
		for(Triangle t : listTriangles){
			
			Vector p = t.getMax();
			
			if(maxPoint.m_X < p.m_X){
				maxPoint.m_X = p.getCoord(0);
			}
			
			if(maxPoint.m_Y < p.m_Y){
				maxPoint.m_Y = p.getCoord(1);
			}
			
			if(maxPoint.m_Z < p.m_Z){
				maxPoint.m_Z = p.getCoord(2);
			}
			
		}
		
		return maxPoint;
		
	}
	
	//Renvoie la liste des triangles intersectes
	public ArrayList<Triangle> getIntersectedTriangle(ArrayList<Triangle> listTriangle){
		
		ArrayList<Triangle> interTriangle = new ArrayList<Triangle>();
		
		for(Triangle t : listTriangle){
			
			if(getBoite().intersectTriangle(t))
				interTriangle.add(t);
			
		}
		
		return interTriangle;
		
	}
	
	//Renvoie la boite du noeud actuel
	public AABB getBoite(){
		
		//Si on a deja cree une fois la boite on a pas besoin de la recalculer
		if(boite != null){
			return boite;
		}
		else{
			
			AABB b = parent.getBoite();
			
			Vector minB = new Vector(0, 0, 0);
			Vector maxB = new Vector(0, 0, 0);
			
			double delta = 0.001;
			
			switch(this.positionId){
				case 1 :
					maxB = b.getMax();
					minB = b.getCenter();
					break;
				case 2 :
					maxB = new Vector(b.getCenter().m_X, b.getMax().m_Y, b.getMax().m_Z);
					minB = new Vector(b.getMin().m_X, b.getCenter().m_Y, b.getCenter().m_Z);
					break;
				case 3 :
					maxB = new Vector(b.getMax().m_X, b.getMax().m_Y, b.getCenter().m_Z);
					minB = new Vector(b.getCenter().m_X, b.getCenter().m_Y, b.getMin().m_Z);
					break;
				case 4 :
					maxB = new Vector(b.getCenter().m_X, b.getMax().m_Y, b.getCenter().m_Z);
					minB = new Vector(b.getMin().m_X, b.getCenter().m_Y, b.getMin().m_Z);
					break;
				case 5 :
					maxB = new Vector(b.getMax().m_X, b.getCenter().m_Y, b.getMax().m_Z);
					minB = new Vector(b.getCenter().m_X, b.getMin().m_Y, b.getCenter().m_Z);
					break;
				case 6 :
					maxB = new Vector(b.getCenter().m_X, b.getCenter().m_Y, b.getMax().m_Z);
					minB = new Vector(b.getMin().m_X, b.getMin().m_Y, b.getCenter().m_Z);
					break;
				case 7 :
					maxB = new Vector(b.getMax().m_X, b.getCenter().m_Y, b.getCenter().m_Z);
					minB = new Vector(b.getCenter().m_X, b.getMin().m_Y, b.getMin().m_Z);
					break;
				case 8 :
					maxB = b.getCenter();
					minB = b.getMin();
					break;
			}
			
			minB.m_X -= delta;
			minB.m_Y -= delta;
			minB.m_Z -= delta;
			
			maxB.m_X += delta;
			maxB.m_Y += delta;
			maxB.m_Z += delta;
			
			boite = new AABB(minB, maxB);
			
			return boite;
			
		}
		
	}
	
	@Override
	public Intersection intersect(Ray ray) {
		
		Intersection inter = null;
		
		double[] minmax = new double[2];
		
		//Si on a intersecte la boite
		if(getBoite().intersectRay(ray, minmax)){
	
			//Si il s'agit d'un noeud parent
			if(enfants != null && enfants.size() > 0){
				
				for(OctreeNode node : enfants){
					
					Intersection tmp = node.intersect(ray);
					
					if(tmp != null && (inter == null || Math.abs(inter.m_RayDistance) > Math.abs(tmp.m_RayDistance))){
						inter = tmp;
					}
					
				}
				
			}
			else if(triangles != null && triangles.size() > 0){ //Sinon si le noeud a des triangles
				
				for(Triangle t : triangles){
					
					Intersection tmp = t.intersect(ray);
					
					if(tmp != null && (inter == null || Math.abs(inter.m_RayDistance) > Math.abs(tmp.m_RayDistance))){
						inter = tmp;
					}
					
					
				}
				
			}
			
		}
		
		return inter;
	}

	
	
	@Override
	public Material getMaterial() {
		return null;
	}

	@Override
	public boolean isSolid() {
		return false;
	}

	@Override
	public ArrayList<Intersection> allIntersect(Ray ray) {

		ArrayList<Intersection> inter = new ArrayList<Intersection>();
		
		double[] minmax = new double[2];
		
		if(getBoite().intersectRay(ray, minmax)){
	
			if(enfants != null && enfants.size() > 0){
				
				for(OctreeNode node : enfants){
					
					inter.addAll(node.allIntersect(ray));
					
				}
				
			}
			else if(triangles != null && triangles.size() > 0){
				
				for(Triangle t : triangles){
					
					Intersection tmp = t.intersect(ray);
					
					if(tmp != null){
						inter.add(tmp);
					}
					
					
				}
				
			}
			
		}
		Outils.triIntersection(inter);
		return inter;
		
	}

	
	
}
