package fc.SimpleRaytracer.Material;

import fc.SimpleRaytracer.Geometry.Intersection;
import fc.SimpleRaytracer.Math.Ray;
import fc.SimpleRaytracer.Math.Vector;
import fc.SimpleRaytracer.Rendering.Camera;
import fc.SimpleRaytracer.Rendering.Color;
import fc.SimpleRaytracer.Rendering.Light;
import fc.SimpleRaytracer.Rendering.Raytracer;
import fc.SimpleRaytracer.Rendering.Scene;
import tools.Outils;

public class MaterialMiroir extends Material{
	
	@Override
	public Color shade(Camera camera, Intersection isect, Light light, Raytracer tracer, double e, Scene scene, int depth) {
		
		if(depth <= Raytracer.MAX_RAYONS_SECONDAIRE){
			
			Vector normal = isect.m_Normal.norm();
			
			Vector point = isect.m_Ray.m_Origin.add(isect.m_Ray.m_Direction.mul(isect.m_RayDistance));
			Vector point2 = isect.m_Ray.m_Origin.add(isect.m_Ray.m_Direction.mul(isect.m_RayDistance)).add(normal.mul(0.001));
			
			Vector dirVue = point.sub(camera.m_Position).norm();
			
			Vector reflexion = Outils.reflect(normal, dirVue).norm();
			
			Ray ray = new Ray(point2, reflexion);
			Color color = tracer.traceRayOneLight(ray, scene, light, depth+1, e);
			
			return color;
		}
		else{
			return new Color(0.1, 0.1, 0.1);
		}
	}

}
