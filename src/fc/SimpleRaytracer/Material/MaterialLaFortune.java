package fc.SimpleRaytracer.Material;

import fc.SimpleRaytracer.Geometry.Intersection;
import fc.SimpleRaytracer.Math.Vector;
import fc.SimpleRaytracer.Rendering.Camera;
import fc.SimpleRaytracer.Rendering.Color;
import fc.SimpleRaytracer.Rendering.Light;
import fc.SimpleRaytracer.Rendering.Raytracer;
import fc.SimpleRaytracer.Rendering.Scene;
import tools.Outils;

public class MaterialLaFortune extends Material{

	Color color;
	double coefDiffuse;
	double coefSpecular;
	double coefMaterial;
	
	Vector coef;
	
	public MaterialLaFortune(Color color, Vector coef){
		
		coefDiffuse = 0.7;
		coefSpecular = 0.3;
		
		coefMaterial = 4;
		
		this.color = color;
		this.coef = coef;
	}
	
	@Override
	public Color shade(Camera camera, Intersection isect, Light light, Raytracer tracer, double e, Scene scene, int depth) {

		Vector normal = isect.m_Normal.norm();
		
		Vector point = isect.m_Ray.m_Origin.add(isect.m_Ray.m_Direction.mul(isect.m_RayDistance));
		
		Vector dirLight = light.m_Position.sub(point).norm();
		
		Vector dirVue = point.sub(camera.m_Position).norm();
		
		Vector reflect = Outils.reflect(normal, dirLight).norm();
		
		double quantiteSpec = reflect.m_X*dirVue.m_X*coef.m_X + reflect.m_Y*dirVue.m_Y*coef.m_Y + reflect.m_Z*dirVue.m_Z*coef.m_Z; 
		quantiteSpec = Math.pow(Math.max(quantiteSpec, 0), coefMaterial);
		
		Color diffuse = this.color.scale(Math.max(Outils.quantiteLumineuse(dirLight, normal), 0.0)).scale(coefDiffuse); 
		
		Color specular = light.m_Color.scale(Math.max(quantiteSpec, 0.0)).scale(coefSpecular);
		
		Color finalColor = diffuse.add(specular);
		
		return finalColor;
	}

}
