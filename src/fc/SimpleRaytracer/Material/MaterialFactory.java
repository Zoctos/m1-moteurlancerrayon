package fc.SimpleRaytracer.Material;

import fc.SimpleRaytracer.Math.Vector;
import fc.SimpleRaytracer.Rendering.Color;

public class MaterialFactory {
	
	public static Material materialColor(Color color){
		return new MaterialColor(color);
	}
	
	public static Material materialBleu(){
		return new MaterialColor(new Color(0.0, 0.0, 1.0));
	}
	
	public static Material materialRouge(){
		return new MaterialColor(new Color(1.0, 0.0, 0.0));
	}
	
	public static Material materialVert(){
		return new MaterialColor(new Color(0.0, 1.0, 0.0));
	}
	
	public static Material materialCyan(){
		return new MaterialColor(new Color(0.0, 1.0, 1.0));
	}
	
	public static Material materialMagenta(){
		return new MaterialColor(new Color(1.0, 0.0, 1.0));
	}
	
	public static Material materialJaune(){
		return new MaterialColor(new Color(1.0, 1.0, 0.0));
	}
	
	public static Material materialBronze(){
		return new MaterialLaFortune(new Color(1.0, 0.6, 0.3), new Vector(0.5, 0.5, 1.5));
	}
	
	public static Material materialPhong(Color color){
		return new MaterialPhong(color);
	}
	
	public static Material materialPlastique(Color color){
		return new MaterialPhong(color, 0.5, 0.66, 16);
	}
	
	public static Material materialEmeraude(){
		return new MaterialLaFortune(new Color(0.0, 1.0, 0.0), new Vector(0.5, 0.5, 1.5));
	}
	
	public static Material materialRubis(){
		return new MaterialLaFortune(new Color(1.0, 0.0, 0.0), new Vector(0.5, 0.5, 1.5));
	}
	
	public static Material materialSaphir(){
		return new MaterialLaFortune(new Color(1.0, 0.0, 0.0), new Vector(0.5, 0.5, 1.5));
	}
	
	public static Material materialLambert(Color color){
		return new MaterialLambert(color);
	}
	
	public static Material materialVectorNormal(){
		return new MaterialVector(MaterialVector.NORMAL);
	}
	
	public static Material materialVectorReflect(){
		return new MaterialVector(MaterialVector.REFLECT);
	}
	
	public static Material materialVectorRefract(){
		return new MaterialVector(MaterialVector.REFRACT);
	}
	
	public static Material materialVerre(){
		return new MaterialVerre();
	}
	
	public static Material materialVerre(Color color){
		return new MaterialVerre(color);
	}
	
	public static Material materialVerre(Color color, double indice){
		return new MaterialVerre(color, indice);
	}

	public static Material materialMiroir(){
		return new MaterialMiroir();
	}
	
}
