package fc.SimpleRaytracer.Material;

import fc.SimpleRaytracer.Geometry.Intersection;
import fc.SimpleRaytracer.Math.Vector;
import fc.SimpleRaytracer.Rendering.Camera;
import fc.SimpleRaytracer.Rendering.Color;
import fc.SimpleRaytracer.Rendering.Light;
import fc.SimpleRaytracer.Rendering.Raytracer;
import fc.SimpleRaytracer.Rendering.Scene;
import tools.Outils;

public class MaterialVector extends Material{

	int type;
	
	public static final int NORMAL = 0;
	public static final int REFLECT = 1;
	public static final int REFRACT = 2;
	
	public MaterialVector(){
		this.type = 0;
	}
	
	public MaterialVector(int type){
		this.type = type;
	}
	
	@Override
	public Color shade(Camera camera, Intersection isect, Light light, Raytracer tracer, double e, Scene scene,
			int depth) {

		Vector normal = isect.m_Normal.norm();
		Vector point = isect.m_Ray.m_Origin.add(isect.m_Ray.m_Direction.mul(isect.m_RayDistance));
		Vector dirVue = point.sub(camera.m_Position).norm();
		
		Vector valeur;
		
		if(type == NORMAL)
			valeur = normal;
		else if(type == REFLECT)
			valeur = Outils.reflect(dirVue, normal);
		else
			valeur = Outils.refract(dirVue, normal, e, 1.524);
		
		return new Color(valeur.m_X, valeur.m_Y, valeur.m_Z);
	}

}
