package fc.SimpleRaytracer.Material;

import fc.SimpleRaytracer.Geometry.Intersection;
import fc.SimpleRaytracer.Math.Ray;
import fc.SimpleRaytracer.Math.Vector;
import fc.SimpleRaytracer.Rendering.Camera;
import fc.SimpleRaytracer.Rendering.Color;
import fc.SimpleRaytracer.Rendering.Light;
import fc.SimpleRaytracer.Rendering.Raytracer;
import fc.SimpleRaytracer.Rendering.Scene;
import tools.Outils;

public class MaterialVerre extends Material{

	Color color;
	double indiceRefraction;
	double coefBrillance;
	double coefDiffuse;
	double coefSpecular;
	
	public MaterialVerre(){
		this.indiceRefraction = 1.545;
		this.color = Color.Grey;
		this.coefBrillance = 16;
		this.coefDiffuse = 0.7;
		this.coefSpecular = 0.3;
	}
	
	public MaterialVerre(Color color){
		this.indiceRefraction = 1.545;
		this.color = color;
		this.coefBrillance = 16;
		this.coefDiffuse = 0.7;
		this.coefSpecular = 0.3;
	}
	
	public MaterialVerre(Color color, double indiceRefraction){
		this.indiceRefraction = indiceRefraction;
		this.color = color;
		this.coefBrillance = 16;
		this.coefDiffuse = 0.7;
		this.coefSpecular = 0.3;
	}
	
	@Override
	public Color shade(Camera camera, Intersection isect, Light light, Raytracer tracer, double e, Scene scene,
			int depth) {
		
		if(depth <= Raytracer.MAX_RAYONS_SECONDAIRE){
			
			Vector normal = isect.m_Normal.norm();
			Vector point = isect.m_Ray.m_Origin.add(isect.m_Ray.m_Direction.mul(isect.m_RayDistance));
			Vector point2 = isect.m_Ray.m_Origin.add(isect.m_Ray.m_Direction.mul(isect.m_RayDistance+0.001));
			
			Vector dirVue = point.sub(isect.m_Ray.m_Origin).norm();
			Vector dirLight = light.m_Position.sub(point).norm();

			Vector reflexion = Outils.reflect(normal, dirLight).norm();
			
			double fresnel;
			Vector refraction;
			
			fresnel = Outils.fresnel(dirVue, normal, dirLight, indiceRefraction);
			refraction = Outils.refract(dirVue, normal, 1, indiceRefraction);
			
			Color diffuse = this.color.scale(Math.max(Outils.quantiteLumineuse(dirLight, normal), 0.0));
			
			Color specular = Color.Background;
			
			double lightQuantity = Math.max(Outils.quantiteLumineuse(dirLight, normal), 0.0);
			if(lightQuantity != 0){
				double val = Math.pow(Math.max(Outils.quantiteLumineuse(dirVue, reflexion), 0), coefBrillance) / lightQuantity;
				
				if(val == Double.POSITIVE_INFINITY)
					val = 0.0;

				specular = light.m_Color.scale(Math.max(val, 0.0)).scale(coefBrillance);
			}
			
			Ray ray = new Ray(point2, refraction);
			Color colorRefract = tracer.traceRayOneLight(ray, scene, light, depth+1, indiceRefraction);
			
			Color colorFinal = diffuse.scale(fresnel).add(colorRefract.scale(1-fresnel)).scale(coefDiffuse).add(specular.scale(coefSpecular));
			
			return colorFinal;
		
		}
		else{
			return this.color;
		}
	}

}
