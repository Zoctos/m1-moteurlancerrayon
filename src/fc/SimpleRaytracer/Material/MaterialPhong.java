package fc.SimpleRaytracer.Material;

import fc.SimpleRaytracer.Geometry.Intersection;
import fc.SimpleRaytracer.Math.Vector;
import fc.SimpleRaytracer.Rendering.Camera;
import fc.SimpleRaytracer.Rendering.Color;
import fc.SimpleRaytracer.Rendering.Light;
import fc.SimpleRaytracer.Rendering.Raytracer;
import fc.SimpleRaytracer.Rendering.Scene;
import tools.Outils;

public class MaterialPhong extends Material{
	
	public double coefDiffuse, coefSpecular;
	public double coefMaterial;
	public Color color;
	
	public MaterialPhong(){
		
		coefDiffuse = 0.7;
		coefSpecular = 0.3;
		coefMaterial = 16;
		
		color = new Color(0.0, 0.5, 1.0);
		
	}
	
	public MaterialPhong(Color color){
		
		coefDiffuse = 0.7;
		coefSpecular = 0.3;
		coefMaterial = 16;
		
		this.color = color;
		
	}
	
	public MaterialPhong(Color color, double coefMaterial){
		
		coefDiffuse = 0.7;
		coefSpecular = 0.3;
		this.coefMaterial = coefMaterial;
		
		this.color = color;
		
	}
	
	public MaterialPhong(Color color, double coefDiffuse, double coefSpecular, double coefMaterial){
		
		this.color = color;
		this.coefDiffuse = coefDiffuse;
		this.coefSpecular = coefSpecular;
		this.coefMaterial = coefMaterial;
		
	}
	
	@Override
	public Color shade(Camera camera, Intersection isect, Light light, Raytracer tracer, double e, Scene scene, int depth) {
		
		Vector normal = isect.m_Normal.norm();
		
		Vector point = isect.m_Ray.m_Origin.add(isect.m_Ray.m_Direction.mul(isect.m_RayDistance));
		
		Vector dirLight = light.m_Position.sub(point).norm();
		
		Vector dirVue = point.sub(camera.m_Position).norm();
		
		Vector reflexion = Outils.reflect(normal, dirLight).norm();
		
		double lightQuantity = Math.max(Outils.quantiteLumineuse(dirLight, normal), 0.0);
		
		Color diffuse = this.color.scale(Math.max(Outils.quantiteLumineuse(dirLight, normal), 0.0)).scale(coefDiffuse);
		Color specular = Color.Background;
		
		if(lightQuantity != 0){
			double val = Math.pow(Math.max(Outils.quantiteLumineuse(dirVue, reflexion), 0), coefMaterial) / lightQuantity;
			
			if(val == Double.POSITIVE_INFINITY)
				val = 0.0;

			specular = light.m_Color.scale(Math.max(val, 0.0)).scale(coefSpecular);
		}
		
		Color finalColor = diffuse.add(specular);
		
		return finalColor;
	}

}
