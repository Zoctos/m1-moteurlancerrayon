package fc.SimpleRaytracer.Material;

import fc.SimpleRaytracer.Geometry.Intersection;
import fc.SimpleRaytracer.Geometry.Triangle;
import fc.SimpleRaytracer.Math.Vector;
import fc.SimpleRaytracer.Rendering.Camera;
import fc.SimpleRaytracer.Rendering.Color;
import fc.SimpleRaytracer.Rendering.Light;
import fc.SimpleRaytracer.Rendering.Raytracer;
import fc.SimpleRaytracer.Rendering.Scene;

public class MaterialLambert extends Material {

	private Color color;
	
	public MaterialLambert(Color color){
		this.color = color;
	}
	
	@Override
	public Color shade(Camera camera, Intersection isect, Light light, Raytracer tracer, double e, Scene scene, int depth) {
		
		Vector normal = isect.m_Normal.norm();
		
		Vector point = isect.m_Ray.m_Origin.add(isect.m_Ray.m_Direction.mul(isect.m_RayDistance));
		
		Vector dirLight = light.m_Position.sub(point).norm();
		
		//On calcule la lumi�re
		Color res = this.color.scale(Math.max(dirLight.dot(normal), 0));
		
		return res;
	}

}
