package fc.SimpleRaytracer.Math;

public class Vector
{
	public double m_X;
	public double m_Y;
	public double m_Z;
	
	public Vector(double x, double y, double z)
	{
		this.m_X = x;
		this.m_Y = y;
		this.m_Z = z;
	}
	
	public Vector(Vector v){
		
		this.m_X = v.m_X;
		this.m_Y = v.m_Y;
		this.m_Z = v.m_Z;
	}
	
	public Vector mul(double k)
	{
		return new Vector(k*m_X, k*m_Y, k*m_Z);
	}
	
	public Vector div(double k)
	{
		return new Vector(m_X/k, m_Y/k, m_Z/k);
	}
	
	public Vector add(Vector v2)
	{
		return new Vector(m_X + v2.m_X, m_Y + v2.m_Y, m_Z + v2.m_Z);
	}
	
	public Vector sub(Vector v2)
	{
		return new Vector(m_X - v2.m_X, m_Y - v2.m_Y, m_Z - v2.m_Z);
	}
	
	public double dot(Vector v2)
	{
		return m_X*v2.m_X + m_Y*v2.m_Y + m_Z*v2.m_Z;
	}
	
	public double length()
	{
		return Math.sqrt(m_X*m_X + m_Y*m_Y + m_Z*m_Z);
	}
	
	public Vector norm()
	{
		double length = length();
		if (length != 0.0)
			return this.div(length); 
		else
			return this;
	}
	
	public Vector cross(Vector v2)
	{
		return new Vector(
				m_Y * v2.m_Z - m_Z * v2.m_Y,
				m_Z * v2.m_X - m_X * v2.m_Z,
				m_X * v2.m_Y - m_Y * v2.m_X);
	}
	
	public double getCoord(int q){
		if(q == 0)
			return m_X;
		if(q == 1)
			return m_Y;
		else
			return m_Z;
	}
	
	public void setCoord(int q, double valeur){
		if(q == 0)
			m_X = valeur;
		if(q == 1)
			m_Y = valeur;
		else
			m_Z = valeur;
	}
	
}

