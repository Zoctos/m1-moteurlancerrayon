package rendering;

import java.awt.image.BufferedImage;

import fc.SimpleRaytracer.MainApplication;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

public class WindowRendering extends Application{
	
	public static ImageView imgView;
	
	public static void startRendering(String[] args, BufferedImage bi){
		imgView = new ImageView();
		imgView.setImage(SwingFXUtils.toFXImage(bi, null));
		launch(args);
	}
	
	@Override
	public void start(Stage primaryStage) throws Exception {
		
		imgView.setFitWidth(MainApplication.widthImage);
		imgView.setFitHeight(MainApplication.heightImage);
		
		primaryStage.setTitle("Rendering");
		
        StackPane root = new StackPane();
        root.getChildren().add(imgView);
        primaryStage.setScene(new Scene(root, MainApplication.widthImage, MainApplication.heightImage));
        primaryStage.show();
		
	}
	
	public static void setImage(){
		Platform.runLater(new Runnable(){
			@Override
			public void run() {
				imgView.setImage(SwingFXUtils.toFXImage(MainApplication.bi, null));
			}
		});
	}
	
}
