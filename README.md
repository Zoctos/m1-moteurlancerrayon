# IMPORTANT #

Réalisé dans le cadre de l'UE Introduction à la Synthèse d'Images Réalistes du Master 1 ISICG. Ce projet avait pour but de nous faire implémenter les termes vu en cours et en TD. Ce projet était réalisé en java et devait permettre de faire un rendu d'une scène 3D en utilisant la méthode du lancer de rayons.

Certaines parties du code nous ont été fourni par notre encadrant de TP/Projet, Frédéric Claux. 

# AUTRES #

[Rapport du projet](http://pierrethuillier.fr/files/rapports/LancerRayon.pdf)